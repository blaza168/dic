<?php
/**
 * Created by PhpStorm.
 * User: Honza
 * Date: 23.10.2017
 * Time: 19:12
 */

class HomePresenter
{
    /**
     * @var Test
     * @inject
     */
    public $test;

    public function talk()
    {
        $this->test->talk();
        $this->getTalker()->talk();
    }

    /**
     * @return Talker
     */
    public function getTalker(): Talker
    {
        return $this->talker;
    }

    /**
     * @param Talker $talker
     */
    public function setTalker(Talker $talker)
    {
        $this->talker = $talker;
    }

    /**
     * @var Talker
     */
    protected $talker;

    /**
     * @param Talker $talker
     */
    public function injectDep(Talker $talker)
    {
        $this->setTalker($talker);
    }
}