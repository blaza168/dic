<?php
/**
 * Created by PhpStorm.
 * User: Honza
 * Date: 23.10.2017
 * Time: 19:12
 */

class Test
{
    /** @var  Talker */
    protected $talker;

    /**
     * Test constructor.
     * @param Talker $talker
     */
    public function __construct(Talker $talker)
    {
        $this->talker = $talker;
    }

    /**
     * @return Talker
     */
    public function getTalker(): Talker
    {
        return $this->talker;
    }

    /**
     * @param Talker $talker
     */
    public function setTalker(Talker $talker)
    {
        $this->talker = $talker;
    }

    public function talk()
    {
        $this->getTalker()->talk();
    }
}