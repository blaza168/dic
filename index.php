<?php

require_once 'DiContainer.php';
require_once 'Talker.php';
require_once 'Test.php';
require_once 'HomePresenter.php';
require_once 'InvalidArgumentException.php';

$di = new \Blaazha\Di\DiContainer();
/** @var Test $presenter */
$presenter = $di->getInstance('HomePresenter');

$presenter->talk();