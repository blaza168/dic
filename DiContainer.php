<?php
namespace Blaazha\Di;

use Blaazha\Exceptions\InvalidArgumentException;

/**
 * Class DiContainer
 * @package Blaazha\Di
 */
class DiContainer
{
	/** @var array  */
	private $services = [];

	/**
	 * @param object $service
	 */
	public function addService($service)
	{
		$key = get_class($service);
		$this->services[$key] = $service;
	}

    /**
     * @param $classWithNamespace
     * @return object
     */
	public function getInstance($classWithNamespace)
	{
		if (!isset($this->services[$classWithNamespace])) {
		    $reflex = new \ReflectionClass($classWithNamespace);

            $constructor = $reflex->getConstructor();
            if (!is_null($constructor)) {
                $parameters = $constructor->getParameters();
                if (!empty($parameters)) {
                    $instance = $reflex->newInstanceArgs($this->getDependencies($parameters));
                } else {
                    $instance = new $classWithNamespace;
                }
            } else {
                $instance = new $classWithNamespace;
            }

			$this->services[$classWithNamespace] = $instance;
			$this->insertDependencies($reflex, $instance);
			return $instance;
		}
		return $this->services[$classWithNamespace];
	}

    /**
     * @param \ReflectionClass $class
     * @return bool
     */
	protected function isPresenter(\ReflectionClass $class)
    {
        $name = $class->getName();
        return mb_strpos(mb_strtolower($name), 'presenter') !== false;
    }

    /**
     * @param array $dependencies
     * @return array
     * @throws InvalidArgumentException
     */
    protected function getDependencies(array $dependencies)
    {
        $parameters = [];
        foreach ($dependencies as $arg) {
            $class = $arg->getClass();
            if ($class !== null) {
                $parameters[] = $this->getInstance($class->getName());
            } else {
                throw new InvalidArgumentException('Please specify class at ' . $arg->getName() . ' argument.');
            }
        }
        return $parameters;
    }

	/**
	 * @param \ReflectionClass $reflect
	 * @param mixed $instance
	 * @throws InvalidArgumentException
	 */
	protected function insertDependencies(\ReflectionClass $reflect, $instance)
	{
		if ($this->isPresenter($reflect)) {
            $properties = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC);
            $methods = $reflect->getMethods(\ReflectionMethod::IS_PUBLIC);

            foreach ($properties as $property) {
                $comment = $property->getDocComment();
                if (mb_strpos(mb_strtolower($comment), '@inject') !== false) {
                    $compliance = [];
                    if (!preg_match('~@var\s+([A-Za-z0-9\\\_\-]+)~u', $comment, $compliance)) {
                        throw new InvalidArgumentException('use @var annotation to specify class of ' . $property->getName() . ' property.');
                    }
                    $typ = $compliance[1];
                    $property->setValue($instance, $this->getInstance($typ));
                }
            }
            foreach ($methods as $method) {
                if (mb_strpos($method->getName(), 'inject') === 0) {
                    $reflection = new \ReflectionMethod($instance, $method->name);
                    $parameters = $this->getDependencies($reflection->getParameters());
                    $reflection->invokeArgs($instance, $parameters);
                }
            }
        }
	}
}